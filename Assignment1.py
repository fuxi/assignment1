import numpy as np
import matplotlib.pyplot as plt

fig=plt.figure()
lambda_r=3
farest = 20
x=np.arange(-farest,farest,0.01)
plt.plot(x, x * 0, color='blue')

for a in range(-farest,farest):

    for r in np.arange(0,farest,lambda_r):
        #The standard formula for a circle
        plt.plot(x, -((r**2-(x-a)**2)**0.5), color ='gray')
    plt.scatter(a, 0, s=100,color='orange')


plt.show()
